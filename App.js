/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import {StyleSheet, Text, View, NativeModules, Button} from 'react-native';

const {CalendarModule} = NativeModules;

const App = () => {
  // for android only
  const onPress = () => {
    console.log('We will invoke the native module here!', CalendarModule);
    CalendarModule.createCalendarEvent('testName', 'testLocation');
  };

  return (
    <View style={styles.Container}>
      <Text style={styles.headText}>Calender Native modules</Text>
      <Button
        title="Click to invoke your native module!"
        color="#841584"
        onPress={onPress}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headText: {
    margin: 20,
  },
});

export default App;
