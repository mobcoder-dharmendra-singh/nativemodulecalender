package com.nativemodulecalender;

import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class CalendarModule  extends ReactContextBaseJavaModule {

    //---3-->>>
    private static ReactApplicationContext reactContext;

    // ---1-->>>
    CalendarModule(ReactApplicationContext context) {
        super(context);
        reactContext=context;
    }

    //---4-->>>
    @ReactMethod
    public void createCalendarEvent(String name, String location) {
        Log.d("CalendarModule", "Create event called with name: " + name
                + " and location: " + location);

    }

    //---2-->>>
    @NonNull
    @Override
    public String getName() {
        return "CalendarModule";
    }
}