//
//  Header.h
//  NativeModuleCalender
//
//  Created by Mobcoder Technologies Private Limited on 19/07/22.
//

#ifndef Header_h
#define Header_h

#import <React/RCTBridgeModule.h>

@interface MyLocationDataManager : NSObject <RCTBridgeModule>
@end


#endif /* Header_h */
